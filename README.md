# NodeCG Twitch Toasts

A example bundle utilizing twitch_events to show a toast notification whenever someone follows the stream.

## Setup

Clone the repository into your `/nodecg/bundles` folder

In your nodecg assets, upload `assets/purple toast cut.png` into the _Split Toast BG_ asset category.
